import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer


somedocs = ['Buffalo buffalo buffalo buffalo buffalo.',
            'The New England Patriots will play in Buffalo on November 11',
            'The American bison, is commonly referred to as the American buffalo or simply "buffalo" in North America']

# set up a list of very simple "documents"
fdocs = ['Apple banana apple', 'banana apple', 'apple orange']

# initialize and "fit" the vectorizer
v = TfidfVectorizer()
mx = v.fit_transform(fdocs)


print(mx.todense()) # sparse matrix
print(pd.DataFrame(mx.todense())) # convert to dataframe
print(v.get_feature_names()) # "feature names" -- the tokens

# data frame of tf/idf bags of words for these "documents"
df = pd.DataFrame(mx.todense(), columns = v.get_feature_names()) 
"""
      apple    banana    orange
0  0.840802  0.541343  0.000000
1  0.613356  0.789807  0.000000
2  0.508542  0.000000  0.861037

"""

# wrap these steps in a function
def get_bag_of_words_tfidf_dataframe(documents):
    tv = TfidfVectorizer()
    return pd.DataFrame(tv.fit_transform(documents).todense(),
                        columns = tv.get_feature_names())
    
# use it
print(get_bag_of_words_tfidf_dataframe(somedocs))
