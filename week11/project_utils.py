import os
import re
import tarfile
import glob
from nltk.tokenize import TreebankWordTokenizer
from gensim.models import KeyedVectors
from random import shuffle
import numpy as np

# Google pre-trained vectors, available here:
# https://code.google.com/archive/p/word2vec/
googlevecs = './data/GoogleNews-vectors-negative300.bin.gz'

word_vectors = KeyedVectors.load_word2vec_format(googlevecs, binary=True, limit=200000)

def pre_process_data(filepath):
    """
    This is dependent on your training data source but we will try to generalize it as best as possible.
    """
    print(filepath)
    positive_path = os.path.join(filepath, 'pos')
    negative_path = os.path.join(filepath, 'neg')

    pos_label = 1
    neg_label = 0

    dataset = []

    for filename in glob.glob(os.path.join(positive_path, '*.txt')):
        with open(filename, 'r') as f:
            dataset.append((pos_label, f.read()))

    for filename in glob.glob(os.path.join(negative_path, '*.txt')):
        with open(filename, 'r') as f:
            dataset.append((neg_label, f.read()))

    shuffle(dataset)

    return dataset


def tokenize_and_vectorize(dataset):
    tokenizer = TreebankWordTokenizer()
    vectorized_data = []
    expected = []
    for sample in dataset:
        tokens = tokenizer.tokenize(sample[1])
        sample_vecs = []
        for token in tokens:
            try:
                sample_vecs.append(word_vectors[token])

            except KeyError:
                pass  # No matching token in the Google w2v vocab

        vectorized_data.append(sample_vecs)

    return vectorized_data

def collect_expected(dataset):
    """ Peel of the target values from the dataset """
    expected = []
    for sample in dataset:
        expected.append(sample[0])
    return expected



def pad_trunc(data, maxlen):
    """ For a given dataset pad with zero vectors or truncate to maxlen """
    new_data = []

    # Create a vector of 0's the length of our word vectors
    zero_vector = []
    for _ in range(len(data[0][0])):
        zero_vector.append(0.0)

    for sample in data:

        if len(sample) > maxlen:
            temp = sample[:maxlen]
        elif len(sample) < maxlen:
            temp = sample
            additional_elems = maxlen - len(sample)
            for _ in range(additional_elems):
                temp.append(zero_vector)
        else:
            temp = sample
        new_data.append(temp)
    return new_data


def preptext(text, maxlen):
    vec_list = tokenize_and_vectorize([(1, text)])
    # Tokenize returns a list of the data (length 1 here)
    test_vec_list = pad_trunc(vec_list, maxlen)
    test_vec = np.reshape(test_vec_list, (len(test_vec_list), maxlen, embedding_dims))
    return test_vec


def predict(text):
    vec_list = tokenize_and_vectorize([(1, text)])
    test_vec_list = pad_trunc(vec_list, maxlen)
    test_vec = np.reshape(test_vec_list, (len(test_vec_list), maxlen, embedding_dims))
    return model.predict_classes(test_vec)
