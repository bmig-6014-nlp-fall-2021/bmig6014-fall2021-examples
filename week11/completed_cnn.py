import glob
from random import shuffle
import os
import re
import tarfile
import tqdm

import numpy as np  # Keras takes care of most of this but it likes to see Numpy arrays
from keras.preprocessing import sequence    # A helper module to handle padding input
from keras.models import Sequential         # The base keras Neural Network model
from keras.layers import Dense, Dropout, Activation   # The layer objects we will pile into the model
from keras.layers import Conv1D, GlobalMaxPooling1D

from pugnlp.futil import path_status, find_files
from nlpia.web import requests_get

from nltk.tokenize import TreebankWordTokenizer
from gensim.models import KeyedVectors


# This is the large movie review dataset, available here:
# https://github.com/gizenmtl/IMDB-Sentiment-Analysis-and-Text-Classification/tree/master/aclImdb
traindata = './data/aclImdb/train'

# Google pre-trained vectors, available here:
# https://code.google.com/archive/p/word2vec/
googlevecs = './data/GoogleNews-vectors-negative300.bin.gz'


def pre_process_data(filepath):
    """
    This is dependent on your training data source but we will try to generalize it as best as possible.
    """
    positive_path = os.path.join(filepath, 'pos')
    negative_path = os.path.join(filepath, 'neg')

    pos_label = 1
    neg_label = 0

    dataset = []

    for filename in glob.glob(os.path.join(positive_path, '*.txt')):
        with open(filename, 'r') as f:
            dataset.append((pos_label, f.read()))

    for filename in glob.glob(os.path.join(negative_path, '*.txt')):
        with open(filename, 'r') as f:
            dataset.append((neg_label, f.read()))

    shuffle(dataset)

    return dataset


word_vectors = KeyedVectors.load_word2vec_format(googlevecs, binary=True, limit=200000)

def tokenize_and_vectorize(dataset):
    tokenizer = TreebankWordTokenizer()

    vectorized_data = []
    expected = []

    for sample in dataset:
        tokens = tokenizer.tokenize(sample[1])

        sample_vecs = []
        for token in tokens:
            try:
                sample_vecs.append(word_vectors[token])
            except KeyError:
                pass # no vector for this token. alternatives: add 0 vector. could we print a message?
             
        vectorized_data.append(sample_vecs)
    return vectorized_data

def collect_expected(dataset):
    """ Peel of the target values from the dataset """
    expected = []
    for sample in dataset:
        expected.append(sample[0])
    return expected


dataset = pre_process_data(traindata)
vectorized_data = tokenize_and_vectorize(dataset)
expected = collect_expected(dataset)



split_point = int(len(vectorized_data) * .8)

x_train = vectorized_data[:split_point]
y_train = expected[:split_point]
x_test = vectorized_data[split_point:]
y_test = expected[split_point:]


maxlen = 400
batch_size = 32         # How many samples to show the net before backpropogating the error and updating the weights
embedding_dims = 300    # Length of the token vectors we will create for passing into the Convnet
filters = 250           # Number of filters we will train
kernel_size = 3         # The width of the filters, actual filters will each be a matrix of weights of size: embedding_dims x kernel_size or 50 x 3 in our case
hidden_dims = 250       # Number of neurons in the plain feed forward net at the end of the chain
epochs = 2              # Number of times we will pass the entire training dataset through the network


def pad_trunc(data, maxlen):
    """ For a given dataset pad with zero vectors or truncate to maxlen """
    new_data = []

    # Create a vector of 0's the length of our word vectors
    zero_vector = []
    for _ in range(len(data[0][0])):
        zero_vector.append(0.0)

    for sample in data:

        if len(sample) > maxlen:
            temp = sample[:maxlen]
        elif len(sample) < maxlen:
            temp = sample
            additional_elems = maxlen - len(sample)
            for _ in range(additional_elems):
                temp.append(zero_vector)
        else:
            temp = sample
        new_data.append(temp)
    return new_data


x_train = pad_trunc(x_train, maxlen)
x_test = pad_trunc(x_test, maxlen)

x_train = np.reshape(x_train, (len(x_train), maxlen, embedding_dims))
y_train = np.array(y_train)
x_test = np.reshape(x_test, (len(x_test), maxlen, embedding_dims))
y_test = np.array(y_test)


model = Sequential()

# we add a Convolution1D, which will learn filters
# word group filters of size filter_length:
model.add(Conv1D(filters,
                 kernel_size,
                 padding='valid',
                 activation='relu',
                 strides=1,
                 input_shape=(maxlen, embedding_dims)))

# we use max pooling:
model.add(GlobalMaxPooling1D())
# We add a vanilla hidden layer:
model.add(Dense(hidden_dims))
model.add(Dropout(0.2))
model.add(Activation('relu'))
# We project onto a single unit output layer, and squash it with a sigmoid:
model.add(Dense(1))
model.add(Activation('sigmoid'))
model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          validation_data=(x_test, y_test))


model_structure = model.to_json()
with open("cnn_model.json", "w") as json_file:
    json_file.write(model_structure)
model.save_weights("cnn_weights.h5")
print('Model saved.')


"""
Put together the tok/vectorize, pad/truncate, and reshaping steps
"""
def makevec(s):
    vl = tokenize_and_vectorize([(1, s)])
    tvl = pad_trunc(vl, maxlen)
    tv = np.reshape(tvl, (len(tvl), maxlen, embedding_dims))
    return tv





"""
# Examples of predicting sentiment on new sentences


ex1 = "The new Dune movie is absolutely amazing! I hope they make a ton more"
ex2 = "The old Dune movie was a terrible and barely worth watching"
model.predict(makevec(ex1))
#array([[0.9836851]], dtype=float32)
model.predict(makevec(ex2))
# array([[0.03809634]], dtype=float32)

from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
analyser = SentimentIntensityAnalyzer()


analyser.polarity_scores(ex1)
# {'neg': 0.0, 'neu': 0.614, 'pos': 0.386, 'compound': 0.82}
analyser.polarity_scores(ex2)
#{'neg': 0.226, 'neu': 0.657, 'pos': 0.117, 'compound': -0.3597}

model.predict(makevec("This was not the best but certainly not the worst."))
#array([[0.03962308]], dtype=float32)

 model.predict(makevec("This was not the best and possibly the  worst."))
#array([[0.03341877]], dtype=float32)
"""
