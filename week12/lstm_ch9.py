from lstm_utils import * # All those helper functions were getting in the way, but now they have their own file
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import LSTM
from tensorflow.keras.optimizers import RMSprop
from keras.layers import Dense, Dropout, Embedding, Flatten, LSTM
from keras.models import model_from_json
import numpy as np



###### Build a model -- Listing 9.1

maxlen = 400
batch_size = 32
embedding_dims = 300
epochs = 2


num_neurons = 50

print('Build model...')
model = Sequential()

model.add(LSTM(num_neurons, return_sequences=True, input_shape=(maxlen, embedding_dims)))
model.add(Dropout(.2))

model.add(Flatten())
model.add(Dense(1, activation='sigmoid'))

model.compile('rmsprop', 'binary_crossentropy', metrics=['accuracy'])
print(model.summary())


###### End Listing 9.1





###### Load and prep data - Listing 9.2
# This actually takes a while - consider putting the result in a pickle

# This is the large movie review dataset, available here:
# https://github.com/gizenmtl/IMDB-Sentiment-Analysis-and-Text-Classification/tree/master/aclImdb
traindata = './data/aclImdb/train'

dataset = pre_process_data(traindata)
vectorized_data = tokenize_and_vectorize(dataset)
expected = collect_expected(dataset)

split_point = int(len(vectorized_data) * .8)

x_train = vectorized_data[:split_point]
y_train = expected[:split_point]
x_test = vectorized_data[split_point:]
y_test = expected[split_point:]


# Also, set up hyperparamters in the middle of data prep -- why not!
maxlen = 400
batch_size = 32         # How many samples to show the net before backpropogating the error and updating the weights
embedding_dims = 300    # Length of the token vectors we will create for passing into the Convnet
epochs = 2

x_train = pad_trunc(x_train, maxlen)
x_test = pad_trunc(x_test, maxlen)

x_train = np.reshape(x_train, (len(x_train), maxlen, embedding_dims))
y_train = np.array(y_train)
x_test = np.reshape(x_test, (len(x_test), maxlen, embedding_dims))
y_test = np.array(y_test)

###### End Listing 9.2




###### Build Keras LSTM - Listing 9.3

print('Build model 2...')
num_neurons = 50
model2 = Sequential()

model2.add(LSTM(num_neurons, return_sequences=True, input_shape=(maxlen, embedding_dims)))
model2.add(Dropout(.2))

model2.add(Flatten())
model2.add(Dense(1, activation='sigmoid'))

model2.compile('rmsprop', 'binary_crossentropy', metrics=['accuracy'])
print(model2.summary())

###### End Listing 9.3



###### Fit the model to the (labeled IMDB) data - Listing 9.4
# Naturally, this is going to take a bunch of time, like three minutes or something
model2.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          validation_data=(x_test, y_test))

###### End Listing 9.4



###### Save the model so we don't have to run this again - Listing 9.5
# It's worth noting: you can build this somewhere else and move it around

model2_structure = model2.to_json()
with open("lstm_model2.json", "w") as json_file:
    json_file.write(model2_structure)

model.save_weights("lstm_weights2.h5")
print('Model 2 saved.')

###### End Listing 9.5



"""
###### Load the model from disk - Listing 9.6
# This is dumb to do when we've just built it and already have it in memory, but a good illustration of how to load models generally

with open("lstm_model2.json", "r") as json_file:
    json_string = json_file.read()
model2 = model_from_json(json_string)

model2.load_weights('lstm_weights2.h5')
###### End Listing 9.6
"""


###### Finally, predict something - Listing 9.7
sample_1 = "I'm hate that the dismal weather that had me down for so long, when will it break! Ugh, when does happiness return?  The sun is blinding and the puffy clouds are too thin.  I can't wait for the weekend."

# We pass a dummy value in the first element of the tuple just because our helper expects it from the way processed the initial data.  That value won't ever see the network, so it can be whatever.
vec_list = tokenize_and_vectorize([(1, sample_1)])

# Tokenize returns a list of the data (length 1 here)
test_vec_list = pad_trunc(vec_list, maxlen)

test_vec = np.reshape(test_vec_list, (len(test_vec_list), maxlen, embedding_dims))

print("Raw output of sigmoid function: {}".format(model2.predict(test_vec)))

###### End Listing 9.7




###### Reload data and optimize vector length - Listing 9.8
# Stop to look at test_len
dataset = pre_process_data(traindata)
vectorized_data = tokenize_and_vectorize(dataset)
test_len(vectorized_data, 400)

###### End listing 9.8



###### Optimize hyperparamters - Listing 9.10
maxlen = 200
batch_size = 32         # How many samples to show the net before backpropagating the error and updating the weights
embedding_dims = 300    # Length of the token vectors we will create for passing into the Convnet

epochs = 2

dataset = pre_process_data(traindata)
vectorized_data = tokenize_and_vectorize(dataset)
expected = collect_expected(dataset)

split_point = int(len(vectorized_data) * .8)

x_train = vectorized_data[:split_point]
y_train = expected[:split_point]
x_test = vectorized_data[split_point:]
y_test = expected[split_point:]

x_train = pad_trunc(x_train, maxlen)
x_test = pad_trunc(x_test, maxlen)

x_train = np.reshape(x_train, (len(x_train), maxlen, embedding_dims))
y_train = np.array(y_train)
x_test = np.reshape(x_test, (len(x_test), maxlen, embedding_dims))
y_test = np.array(y_test)




#Build it 
num_neurons = 50

print('Build model3...')
model3 = Sequential()

model3.add(LSTM(num_neurons, return_sequences=True, input_shape=(maxlen, embedding_dims)))
model3.add(Dropout(.2))

model3.add(Flatten())
model3.add(Dense(1, activation='sigmoid'))

model3.compile('rmsprop', 'binary_crossentropy', metrics=['accuracy'])
print(model3.summary())
###### End Listing 9.11



###### Fit it - Listing 9.12
# Note that this is quicker that fitting the last one
model3.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          validation_data=(x_test, y_test))

# And write it to files
model3_structure = model3.to_json()
with open("lstm_model3.json", "w") as json_file:
    json_file.write(model3_structure)

model3.save_weights("lstm_weights7.h5")
print('Model 3 saved.')


print(f"Predicting class for '{sample_1}'")
print(f'Model #1 says: {model.predict(preptext(sample_1, 400))}')
print(f'Model #2 says: {model2.predict(preptext(sample_1, 400))}')
print(f'Model #3 says: {model3.predict(preptext(sample_1, 200))}')
