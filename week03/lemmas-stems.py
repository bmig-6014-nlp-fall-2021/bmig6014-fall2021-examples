import spacy 
from nltk.stem import *

ps = PorterStemmer()

# Requires one-time download (per virtualenv, of course) of spacy English models
# See quickstart instructions here: https://spacy.io/usage/models
nlp = spacy.load('en_core_web_sm') # spacy english nlp object

sent = "I was running the 5k run when I saw the three candidates in the running for president and several who ran previously"
toks = sent.split()

print(f"Stem 'runs': {ps.stem('runs')}")
print(f"Stem 'running': {ps.stem('running')}")
print(f"Stem 'ran': {ps.stem('ran')}")


print("\nStem the sentence: ")
print(' '.join([ps.stem(w) for w in toks]))

print("\nStem a bunch of words with similar prefixes: ")
print([ps.stem(w) for w in "severe severence severally severed my income".split()])


x = nlp(sent) # do the spacy nlp to this sentence
print("\nPrint the tokens spacy nlp made: ")
print([t for t in x])

print("\nPrint spacy nlp's lemmas for this sentence: ")
print([t.lemma_ for t in x])

# print out the tokens, lemmas, and parts of speech
for t in x:
    print(f"The token is '{t}' and its lemma is '{t.lemma_} and its pos is {t.pos_}'")
