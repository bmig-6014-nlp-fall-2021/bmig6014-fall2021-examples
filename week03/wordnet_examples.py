import nltk
from nltk.corpus import wordnet


print(wordnet.synsets('drink')) # find synsets with the term 'drink'
print(wordnet.synset('drink.v.01')) # look up a particular synset

drink = wordnet.synset('drink.v.01')
print(drink.hyponyms())  # get the hyponyms for this synset

print(drink.lemma_names()) # lemmas for 'drink' synset


# "flatten" list comprehension:
mylist = [ [1,2,3], ["a", "b", "c"], [4,5,6]]
print([item for sublist in mylist for item in sublist])

# get the closure on the hyponym relation - this builds a generator
hypos = drink.closure(lambda x : x.hyponyms())
print(list(hypos))


