import nltk
import pandas as pd
import collections
from nltk import casual_tokenize
import itertools
import gensim

inf = './1557tweets.csv'
nltk.download('stopwords')
sw = nltk.corpus.stopwords.words('english')

entries = pd.read_csv(inf).text

def flatten(l):
    return [x for x in itertools.chain.from_iterable(l)]

# a list of lists of tokens (strings). each inner list is the tokens for a sentence
toksents = [nltk.casual_tokenize(s) for s in entries]


model1 = gensim.models.Word2Vec(toksents,
                                vector_size=150,    # our vectors will have 150 features 
                                window=5,    # how far to look from each word when training
                                min_count=2, # "Ignores all words with total frequency lower than this."
                                workers=10,  # threads
                                epochs=1)      # how many iterations. 1 is going to be too few

model150 = gensim.models.Word2Vec(toksents,
                                vector_size=150,
                                window=5,   
                                min_count=2,
                                workers=10,
                                epochs=150)

"""
>>> model150.wv.most_similar('codeine')
[('Codeine', 0.607058584690094), ('sprite', 0.4626070261001587), ('💥', 0.4124632775783539), ('lean', 0.38835325837135315), ('remember', 0.3808598220348358), ('Mixed', 0.3664655089378357), ('Wake', 0.3615155816078186), ('quit', 0.3604547083377838), ('acetaminophen', 0.3526458442211151), ('Mary', 0.3522709310054779)]

>>> model1.wv.most_similar('lean')
[('what', 0.2981179654598236), ('good', 0.28924867510795593), ('why', 0.2606293559074402), ('I', 0.2538106143474579), ('i', 0.24550625681877136), ('a', 0.23623137176036835), ('treat', 0.23486565053462982), ('🎶', 0.23261407017707825), ('codeine', 0.22983326017856598), ('opium', 0.22915485501289368)]

>>> model150.wv.most_similar(['lean'])
[('sippin', 0.6128149032592773), ('bleed', 0.5885447263717651), ('hella', 0.5790121555328369), ('lines', 0.5639455318450928), ('arms', 0.5544516444206238), ("She's", 0.5382834076881409), ('serve', 0.5246194005012512), ('drink', 0.5227697491645813), ('never', 0.5100042223930359), ('Got', 0.5056561827659607)]

>>> model150.wv.most_similar(['lean'], negative = ['drink'])
[('…', 0.3966980576515198), ('arms', 0.39191555976867676), ('O', 0.38775834441185), ('🎶', 0.3828059136867523), ('Got', 0.3815065920352936), ('couple', 0.36708059906959534), ('pa', 0.36472195386886597), ('moment', 0.3647192716598511), ('er', 0.3623121678829193), ('birth', 0.3436274230480194)]
"""
