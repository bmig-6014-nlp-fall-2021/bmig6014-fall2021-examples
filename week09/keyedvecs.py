from gensim.models import KeyedVectors

"""
Pretrained word vectors available as GoogleNews-vectors-negative300.bin.gz on this page:

https://code.google.com/archive/p/word2vec/
"""

word_vectors = KeyedVectors.load_word2vec_format('./GoogleNews-vectors-negative300.bin.gz', binary = True)

"""
>>> word_vectors.most_similar('lean')
[('Trakware_specializes', 0.5659352540969849), ('Lean', 0.5564606785774231), ('leaner', 0.5395914912223816), ('unto_thine_own', 0.4945884346961975), ('described_compulsive_exerciser', 0.49305811524391174), ('revolutionary_digital_kanban', 0.487265020608902), ('leaning', 0.4853687882423401), ('leans', 0.4853370785713196), ('agile', 0.46902158856391907), ('lean_physique', 0.46796751022338867)]

>>> word_vectors.most_similar('codeine')
[('containing_codeine', 0.6056866645812988), ('Codeine', 0.5917508006095886), ('alprazolam_Xanax', 0.5695599913597107), ('codeine_cough_syrup', 0.5654269456863403), ('cough_syrup', 0.5610266327857971), ('morphine', 0.5592806935310364), ('codeine_syrup', 0.5540850162506104), ('phenobarbital', 0.5470989942550659), ('prescription_cough_syrup', 0.5444708466529846), ('hydrocodone', 0.5402147769927979)]
>>> 

"""
