import spacy
import pandas as pd
import random

infile = './ade-texts.txt'

with open(infile, 'r') as inf:
    lines = inf.readlines()



print(f"Read {len(lines)} lines from the file.")

l = random.choice(lines)
print(f"Picked a random line: '{l}'")

line267 = lines[267] 
print(f"Line 267: '{line267}'")


"""
If this fails with an error like the following, then you need to download the model
    OSError: [E050] Can't find model 'en_ner_bionlp13cg_md'. It doesn't seem to be a Python package or a valid path to a data directory.


Depending on how your environment is set up, there may be different ways of downloading this model into your environment. 
If I'm working with a virtual environment and have activated that virtual environment in a bash shell, then I can install it to that environment with the following command:
    bash-3.2$ pip install https://s3-us-west-2.amazonaws.com/ai2-s2-scispacy/releases/v0.4.0/en_ner_bionlp13cg_md-0.4.0.tar.gz

Note that the part before the $ is the bash prompt, not part of the command to be typed.

"""

# see here for scispacy models https://allenai.github.io/scispacy/
bionlp = spacy.load("en_ner_bionlp13cg_md")
print("\nscispacy's en_ner_bionlp13cg_md model found the following entities for '%s': %s" % (l, bionlp(l).ents))
print("\nscispacy's en_ner_bionlp13cg_md model found the following entities for '%s': %s" % (line267, bionlp(line267).ents))
