import pandas as pds
import gensim
import nltk

notes = 'ade-texts.txt'

documents = []
with open(notes, 'r') as f:
    for i,line in enumerate (f):
        documents.append(nltk.word_tokenize(line.lower()))

# train a lousy w2v model
model10 = gensim.models.Word2Vec(documents,
                                 vector_size=150,
                                 window=6,
                                 min_count=2,
                                 workers=10,
                                 epochs=10)
# train a better w2v model
model100 = gensim.models.Word2Vec(documents,
                                  vector_size=150,
                                 window=6,
                                 min_count=2,
                                 workers=10,
                                 epochs=100)

print(model100.wv.most_similar('hypertension'))
"""
[('lymphopenia', 0.5626562833786011), ('chest', 0.5504204034805298), ('elevation', 0.5244044661521912), ('fatigue', 0.5158429145812988), ('develops', 0.5140230655670166), ('jaundice', 0.5108762979507446), ('hematoma', 0.5079687833786011), ('confusion', 0.5054360628128052), ('tightness', 0.5039811730384827), ('ascites', 0.4907725155353546)]
"""

print(model10.wv.most_similar('hypertension'))
"""
[('when', 0.9997586607933044), ('at', 0.9997203946113586), ('other', 0.9996960759162903), ('within', 0.9996957778930664), ('diffuse', 0.9996926784515381), ('was', 0.9996897578239441), ('levels', 0.9996855854988098), ('were', 0.9996851682662964), ('including', 0.9996821880340576), ('liver', 0.9996815919876099)]
"""

print(model100.wv.most_similar('lidocaine'))
"""
[('operative', 0.8674412965774536), ('bupivacaine', 0.7783215641975403), ('cream', 0.6277592182159424), ('vaginal', 0.6263236403465271), ('additional', 0.6156827211380005), ('cataract', 0.5928829312324524), ('axillary', 0.5874431729316711), ('apneic', 0.5677931904792786), ('injection', 0.5666990876197815), ('vaginosis', 0.5666821599006653)]
"""
