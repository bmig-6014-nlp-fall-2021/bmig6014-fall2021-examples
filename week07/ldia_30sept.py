import pandas as pd
import string
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import LatentDirichletAllocation as LDiA
from nltk.tokenize import casual_tokenize
from langdetect import detect, detect_langs
from nltk.corpus import stopwords

infile = './All_Articles_Excel_Dec2019July2020.xlsx'
adf = pd.read_excel(infile, dtype = {'Abstract' : str})
adf = adf[~adf.Abstract.isna()]

# Make a list of stopwords
stops = stopwords.words('english') + list(string.punctuation)

# Returns true if its input is English text and false otherwise
def is_english(text, thresh = .9):
    try:
        langs = detect_langs(text)
        return {l.lang : l.prob for l in langs}['en'] > thresh
    except:
        return False

# get 1000 abstracts
endf = adf[['Abstract']].copy()[:1000]
# create a column indicating which abstracts are English
endf['English'] = endf.Abstract.apply(is_english)
# rebuild the df with just English abstracts
endf = endf[endf.English]

# build the vectorizer -- recall that the results are very different with/without stopwords
tfidf_model = TfidfVectorizer(stop_words = stops)
# Use the following line instead to limit the number of tokens the vectorizer will use. This makes a big difference!
# tfidf_model = TfidfVectorizer(stop_words = stops, max_features = 1000)
tfidf_docs = tfidf_model.fit_transform(raw_documents=endf.Abstract).toarray()

# n_components is the number of topics. Play around with this.
lda = LDiA(n_components = 10) 
lda.fit(tfidf_docs)


"""
 Adapted from NLPIA

Example usage: 
print_topics(lda, tfidf_model.get_feature_names())
"""
def print_topics(lda_model, vocab, n_top_words = 10):
    for topic_idx, topic in enumerate(lda_model.components_):
        print("\nTopic #%d:" % topic_idx)
        print(" ".join([vocab[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]))

# document/topics matrix        
doc_topics = pd.DataFrame(lda.transform(tfidf_docs))
# topics/words matrix
topic_words = pd.DataFrame(lda.components_, columns = tfidf_model.get_feature_names())

"""
Example usage:
  get_topic_words(topic_words, 5)
"""
def get_topic_words(topic_wordsdf, topic_num, n_top_words = 10):
    """Given a topic/words dataframe, return the n_top_words for the topic topic_num, 
       as a pandas.Series whose indices are the words"""
    return topic_wordsdf.iloc[topic_num].sort_values()[-n_top_words:]

"""
Example usage:
  get_topic_docs(doc_topics, 5)
"""
def get_topic_docs(doc_topics, topic_num, n_top_docs = 10):
    """Given a document/topics dataframe, return the n_top_docs documents for the topic topic_num, 
       as a pandas.Series whose indices are document numbers"""
    return doc_topics[topic_num].sort_values()[-n_top_docs:]
