import glob
from random import shuffle
import os
import re
import tarfile
import tqdm

import numpy as np  # Keras takes care of most of this but it likes to see Numpy arrays
from keras.preprocessing import sequence    # A helper module to handle padding input
from keras.models import Sequential         # The base keras Neural Network model
from keras.layers import Dense, Dropout, Activation   # The layer objects we will pile into the model
from keras.layers import Conv1D, GlobalMaxPooling1D

from pugnlp.futil import path_status, find_files
from nlpia.web import requests_get

from nltk.tokenize import TreebankWordTokenizer
from gensim.models import KeyedVectors


# This is the large movie review dataset, available here:
# https://github.com/gizenmtl/IMDB-Sentiment-Analysis-and-Text-Classification/tree/master/aclImdb
traindata = './data/aclImdb/train'

# Google pre-trained vectors, available here:
# https://code.google.com/archive/p/word2vec/
googlevecs = './data/GoogleNews-vectors-negative300.bin.gz'

def pre_process_data(filepath):
    """
    This is dependent on your training data source but we will try to generalize it as best as possible.
    """
    positive_path = os.path.join(filepath, 'pos')
    negative_path = os.path.join(filepath, 'neg')

    pos_label = 1
    neg_label = 0

    dataset = []

    for filename in glob.glob(os.path.join(positive_path, '*.txt')):
        with open(filename, 'r') as f:
            dataset.append((pos_label, f.read()))

    for filename in glob.glob(os.path.join(negative_path, '*.txt')):
        with open(filename, 'r') as f:
            dataset.append((neg_label, f.read()))

    shuffle(dataset)

    return dataset


word_vectors = KeyedVectors.load_word2vec_format(googlevecs, binary=True, limit=200000)

def tokenize_and_vectorize(dataset):
    tokenizer = TreebankWordTokenizer()

    vectorized_data = []
    expected = []

    for sample in dataset:
        tokens = tokenizer.tokenize(sample[1])

        sample_vecs = []
        for token in tokens:
            try:
                sample_vecs.append(word_vectors[token])
            except KeyError:
                pass # no vector for this token. alternatives: add 0 vector. could we print a message?
             
        vectorized_data.append(sample_vecs)
    return vectorized_data

def collect_expected(dataset):
    """ Peel of the target values from the dataset """
    expected = []
    for sample in dataset:
        expected.append(sample[0])
    return expected


vectorized_data = tokenize_and_vectorize(dataset)
expected = collect_expected(dataset)
