import json

infname = './sample/0017447a237cee0445a83538a538e7c6e9336de6.json'
with open(infname, 'r') as inf:
    data = json.load(inf)

"""
# data is a dictionary. let's look at its keys
>>> data.keys()
dict_keys(['paper_id', 'metadata', 'abstract', 'body_text', 'bib_entries', 'ref_entries', 'back_matter'])

# 'abstract' within this dictionary is a list containing one element, which is itself a dict:
>>> type(data['abstract'])
<class 'list'>
>>> len(data['abstract'])
1
>>> type(data['abstract'][0])
<class 'dict'>
>>> data['abstract'][0].keys()
dict_keys(['text', 'cite_spans', 'ref_spans', 'section', 'termite_hits'])

# and 'termite_hits' within that is also a dict
>>> type(data['abstract'][0]['termite_hits'])
<class 'dict'>

# its keys look like the names of entity types:
>>> data['abstract'][0]['termite_hits'].keys()
dict_keys(['SARSCOV', 'GENE', 'CORONAPROT', 'HPO', 'SPECIES', 'INDICATION', 'GOONTOL'])

# if we look at those, we get what looks like a  annotations
>>> data['abstract'][0]['termite_hits']['SARSCOV']
[{'hit_count': 5, 'name': 'Severe acute respiratory syndrome coronavirus 2', 'id': 'http://purl.obolibrary.org/obo/NCBITaxon_2697049', 'hit_sentences': [1, 2, 3, 9, 9], 'hit_sentence_locations': [[0, 141], [142, 290], [291, 443], [1079, 1259], [1079, 1259]]}]

# that thing ^ is a list of length 1, so we'll just look at the first item in that list:
>>> data['abstract'][0]['termite_hits']['SARSCOV'][0]
{'hit_count': 5, 'name': 'Severe acute respiratory syndrome coronavirus 2', 'id': 'http://purl.obolibrary.org/obo/NCBITaxon_2697049', 'hit_sentences': [1, 2, 3, 9, 9], 'hit_sentence_locations': [[0, 141], [142, 290], [291, 443], [1079, 1259], [1079, 1259]]}

# and that thing ^ is a dict. what are its keys?
>>> data['abstract'][0]['termite_hits']['SARSCOV'][0].keys()
dict_keys(['hit_count', 'name', 'id', 'hit_sentences', 'hit_sentence_locations'])

# let's look at a hit_sentence_location for a GENE:
>>> data['abstract'][0]['termite_hits']['GENE'][0]['hit_sentence_locations'][0]
[142, 290]

# these are the start and end characters for the sentence that mentions a GENE:
>>> (start, end) = data['abstract'][0]['termite_hits']['GENE'][0]['hit_sentence_locations'][0]
>>> start
142
>>> end 
290

# here's the abstract text again:
>>> data['abstract'][0]['text']
"SARS-CoV-2 infection is characterized by a protean clinical picture that can range from asymptomatic patients to life-threatening conditions. Severe COVID-19 patients often display a severe pulmonary involvement and develop neutrophilia, lymphopenia, and strikingly elevated levels of IL-6. There is an over-exuberant cytokine release with hyperferritinemia leading to the idea that COVID-19 is part of the hyperferritinemic syndrome spectrum. Indeed, very high levels of ferritin can occur in other diseases including hemophagocytic lymphohistiocytosis, macrophage activation syndrome, adult-onset Still's disease, catastrophic antiphospholipid syndrome and septic shock. Numerous studies have demonstrated the immunomodulatory effects of ferritin and its association with mortality and sustained inflammatory process. High levels of free iron are harmful in tissues, especially through the redox damage that can lead to fibrosis. Iron chelation represents a pillar in the treatment of iron overload. In addition, it was proven to have an anti-viral and anti-fibrotic activity. Herein, we analyse the pathogenic role of ferritin and iron during SARS-CoV-2 infection and propose iron depletion therapy as a novel therapeutic approach in the COVID-19 pandemic."

# let's use the start and end that we set up to pull out the sentence mentioning the GENE:
>>> data['abstract'][0]['text'][start:end]
'Severe COVID-19 patients often display a severe pulmonary involvement and develop neutrophilia, lymphopenia, and strikingly elevated levels of IL-6.'
"""
